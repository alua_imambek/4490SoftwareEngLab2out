package lab2out;

import java.io.IOException;
import java.net.InetAddress;
import java.util.Scanner;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.UIManager;


import ocsf.server.AbstractServer;
import ocsf.server.ConnectionToClient;

public class ChatServer extends AbstractServer
{
    private JTextArea log; //Corresponds to JTextArea of ServerGUI
    private JLabel status; //Corresponds to the JLabel of ServerGUI
    
    public ChatServer()
    {
      super(12345);
      super.setTimeout(500);
      
    }
    public void setLog(JTextArea log)
    {
      this.log = log;
    }
    public void setStatus(JLabel status)
    {
      this.status = status;
    }
    public void handleMessageFromClient(Object arg0, ConnectionToClient arg1)
    {
        System.out.println("Message from Client Received");
    }
    public void listeningException(Throwable exception) 
    {
//    	System.out.println("Listening Exception Occurred");
//      System.out.println(exception.toString());
      exception.printStackTrace();
      log.append(exception.toString());
    }
      
    public void serverStarted() 
    { 
     log.append("Server Started\n");
     this.status.setText("Listening");
     this.status.setForeground(Color.green);
    }
    
    public void serverStopped()
    {
      this.log.append("Server Stopped Accepting New Clients - Press Listen to Start Accepting New Clients\n");
      this.status.setText("Stopped");
      this.status.setForeground(Color.red);
    }
    public void serverClosed()
    {
      this.log.append("Server and all current clients are closed - Press Listen to Restart\n");
      this.status.setText("Close");
      this.status.setForeground(Color.red); 
    }
    public void clientConnected(ConnectionToClient client)
    {
      this.log.append("Client connected\n");

    }
}
