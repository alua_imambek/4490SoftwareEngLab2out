package lab2out;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.UIManager;
import java.io.IOException;


public class ServerGUI extends JFrame
{
    private JLabel status;	
    private JButton listen;	
    private JButton close;	
    private JButton stop;	
    private JButton quit;	
    private String[] labels = {"Port", "Timeout"};
    private JTextField[] textFields = new JTextField[labels.length];
    private JTextArea log;
    private ChatServer server;
    boolean flag;
    
    private class EventHandler implements ActionListener
    {
        public void actionPerformed(ActionEvent e) 
        {
            String command = e.getActionCommand();  
            server.setLog(log);
            server.setStatus(status);
            if(command.equals("Quit"))
            	{
            		System.exit(0);
            	}
            else if(command.equals("Listen"))
            {
              if(textFields[0].getText().equals("") || textFields[1].getText().equals(""))
              {
                log.append("Port Number/timeout not entered before pressing Listen\n");
              }
              else
              {
                try
                {
                  server.listen();
                } catch (IOException e1)
                {
                  e1.printStackTrace();
                }
                flag = true;
                
                server.serverStarted();       
              }
            }
            else if((command.equals("Close") || command.equals("Stop")) && flag == false )
            {
              log.append("Server not currently started\n");
            }
            else if( command.equals("Stop") && flag == true )
            {
              server.serverStopped();
              flag = false;
            }
            else if(command.equals("Close") && flag == true )
            {
              server.serverClosed();
              flag = false;
            }
            
        }
    }
    
    public ServerGUI(String title)
    {
      this.flag = false;
      this.server = new ChatServer();
      this.setTitle(title);
      this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
      
      //North Panel
      JPanel north = new JPanel(new FlowLayout());     
      this.getContentPane().add(north, BorderLayout.NORTH);
      
      //Connection label
      this.status = new JLabel("Not Connected");
      this.status.setForeground(Color.red);
      north.add(status);
      
      //South Panel
      JPanel south = new JPanel(new FlowLayout());
      this.getContentPane().add(south, BorderLayout.SOUTH);
      
      //4 Buttons
      this.listen  = new JButton("Listen");
      this.close   = new JButton("Close");
      this.stop    = new JButton("Stop");
      this.quit    = new JButton("Quit");
      
      //Event Handling 
      this.listen.addActionListener(new EventHandler ()); 
      this.close.addActionListener (new EventHandler ()); 
      this.stop.addActionListener  (new EventHandler ()); 
      this.quit.addActionListener  (new EventHandler ()); 
      south.add(this.listen);
      south.add(this.close);
      south.add(this.stop);
      south.add(this.quit);
      
      //Center Panel
      JPanel center = new JPanel();
      center.setLayout( new BoxLayout(center, BoxLayout.Y_AXIS) );
      this.getContentPane().add(center, BorderLayout.CENTER);
      
      
      //Text fields
      JPanel panel1 = new JPanel(new FlowLayout());            
      JPanel panel2 = new JPanel(new FlowLayout());            
                                                               
      JLabel panel1_label = new JLabel(labels[0] + "      ");  
      panel1_label.setHorizontalAlignment(SwingConstants.LEFT);
      panel1.add(panel1_label);                                
      textFields[0] = new JTextField(15);                      
      textFields[0].setHorizontalAlignment(JTextField.LEADING);
      panel1.add( textFields[0] );                             
                                                               
      JLabel panel2_label = new JLabel(labels[1]);             
      panel2_label.setHorizontalAlignment(SwingConstants.LEFT);
      panel2.add(panel2_label);                                
      textFields[1] = new JTextField(15);                      
      textFields[1].setHorizontalAlignment(JTextField.LEADING);
      panel2.add( textFields[1] );                             
                                                               
      center.add(panel1);                                      
      center.add(panel2);                                      
      
      
      // Log Area
      center.add( new JLabel("Server Log") );
      log = new JTextArea(5,20);
      log.setEditable(false);
      JScrollPane sa_scrollP = new JScrollPane(log);
      center.add(sa_scrollP);
      
      // Sides
      JPanel west = new JPanel(new FlowLayout());
      west.add(Box.createRigidArea(new Dimension(5,0)));
      this.getContentPane().add(west, BorderLayout.WEST);
      JPanel east = new JPanel(new FlowLayout());
      east.add(Box.createRigidArea(new Dimension(5,0)));
      this.getContentPane().add(east, BorderLayout.EAST);
      
      // No requirement for resizing
      this.setResizable(false);
      
      this.pack();
      this.setVisible(true);  
    }
    
    public JTextField getTextFieldAt(int index)
    {
      return textFields[index];
    }
    
    public JLabel getStatus()
    {
      return status;
    }
    
    public JTextArea getLog()
    {
      return log;
    }

    public static void main(String[] args)
    {
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } 
        catch (UnsupportedLookAndFeelException e){}
        catch (ClassNotFoundException e) {}
        catch (InstantiationException e) {}
        catch (IllegalAccessException e) {}
        
        new ServerGUI(args[0]); //args[0] represents the title of the GUI
    }
}